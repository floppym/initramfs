#!/bin/sh
dirname=${0%/*}
init=${dirname}/init

getlibs() {
	depfile="$(mktemp)"

	for x; do
		ldd "$x" | awk '
		$1 ~ /^\// { print $1 }
		$2 == "=>" { print $3 }
		' >> "${depfile}"
	done

	libs="$(sort -u "${depfile}")"
	rm -f "${depfile}"

	for libname in ${libs}; do
		basename=${libname##*/}
		dirname=${libname%/*}
		libdir=${dirname##*/}
		printf "file\t%s\t%s\t0755 0 0\n" "/${libdir}/${basename}" "${libname}"
	done
}

cat <<EOF
# Basic initramfs
# vim:tw=0 ts=8 noet

dir	/bin		0755 0 0
dir	/dev		0755 0 0
dir	/etc		0755 0 0
dir	/lib64		0755 0 0
dir	/newroot	0755 0 0
dir	/proc		0755 0 0
dir	/sys		0755 0 0

nod	/dev/console	0600 0 0 c 5 1
nod	/dev/null	0666 0 0 c 1 3

file	/init	${init}	0755 0 0

file 	/bin/busybox 	/bin/busybox	0755 0 0 /bin/sh
file	/bin/btrfs	/sbin/btrfs	0755 0 0

EOF
getlibs /bin/busybox /sbin/btrfs
