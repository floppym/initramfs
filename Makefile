KBUILD_OUTPUT ?= /usr/src/linux
PATH += $(KBUILD_OUTPUT)/usr
GEN_INIT_CPIO ?= gen_init_cpio

initramfs.img: initramfs.txt init
	$(GEN_INIT_CPIO) initramfs.txt | gzip > initramfs.img

initramfs.txt: initramfs.sh
	./initramfs.sh > initramfs.txt

.PHONY: install

install: initramfs.img
	-cp /boot/initramfs.img /boot/initramfs.img.old
	cp initramfs.img /boot/initramfs.img
